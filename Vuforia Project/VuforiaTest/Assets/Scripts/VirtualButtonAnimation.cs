﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class VirtualButtonAnimation : MonoBehaviour, IVirtualButtonEventHandler {

    public GameObject vbBtnObj;
    public Animator spiderAnim;

	// Use this for initialization
	void Start () {
        vbBtnObj = GameObject.Find("VirtualButtonDon'tTocuh");
        vbBtnObj.GetComponent<VirtualButtonBehaviour>().RegisterEventHandler(this);
        spiderAnim.GetComponent<Animator>();
    }
	
    public void OnButtonPressed(VirtualButtonBehaviour vb)
    {
        spiderAnim.Play("SpiderMove2");
        Debug.Log("Button Pressed");
    }

    public void OnButtonReleased(VirtualButtonBehaviour vb)
    {
        spiderAnim.Play("none");
        Debug.Log("Button Released");
    }
}
