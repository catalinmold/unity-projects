# Maze Generator
Generates a maze by passing the width and height size. The character has to collect all FoodCubes from the maze. The number of FoodCubes depends on how big the size of the maze is.

# Functionality
Arrows / WASD: Moving in any direction.
Q: Change view perspective.